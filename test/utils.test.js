import utils from '../src/helpers/utils.js';
import chai from 'chai';

const {assert} = chai;

describe('utils', ()=>{
  describe('#argmaxThresh', ()=>{
    it('should give correct argmax with threshold', (done)=>{
      assert.sameMembers(
        utils.argmaxThresh([0, 0, 1, 0.3, 0.6, 0.5, 0.9999], 0.56), [2, 4, 6]);
      done();
    })
  });

  describe('#randomBytes', ()=>{
    it('should create random values', (done)=>{
      const rand1 = utils.randomBytes(8);
      const rand2 = utils.randomBytes(8);
      assert.notEqual(rand1, rand2);
      done()
    });
  });
});
