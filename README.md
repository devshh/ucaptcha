[![Build Status](https://travis-ci.com/devshh/uCaptcha.svg?branch=master)](https://travis-ci.com/devshh/uCaptcha)

# Introduction
uCaptcha is a simple drop-in replacement for reCaptcha by Google.
It has much the same API and has an additional benefit of helping [OpenStreetCam](https://openstreetcam.org) label their images. It can be self hosted relatively easily, however you can also use our hosted version if you'd like.


# Donate
If you use uCaptcha in production, or want to see the project grow, consider funding the project to help pay for hosting and bandwidth costs.


XMR: `45uYweaLaasTFKdEHK9qADRZMatRE3U9vKFwT1kpaig26GnruZm1t21ipVjsC1KLeeL7sG3m8bHfhcce4tQJLDKNCFBLPar`


BTC: `bc1qjj45cuu7d8r6g83al7yrtkkmzu6ud4j9qzq3dn`


# TODO
## Short term (1 - 2 weeks)
- [ ] Think of a way to detect if user's selection is correct answer
- [ ] Create tests
- [ ] Configure Rollup to create 2 packages 1x esm 1x cjs
- [ ] Improve the UI

## Medium term (> 1 month)
- [ ] Increase security and accuracy of predictions

## Long term (> 3 months)
Nothing here
