import db from './helpers/db/index.js';
import {app} from './server.js';

import {fetchImagesJob} from './helpers/fetchImagesJob.js';
import {fetchImages} from './helpers/fetchImagesJob.js';
import logger from './helpers/logging.js';

db.connect((err)=>{
  if (err) {
    console.error(err);
    process.exit(1);
  }
  app.listen(process.env.PORT, ()=>{
    fetchImagesJob.start();
    fetchImages({radius: 500});
    logger.info('Server started');
    logger.info('=========================================================');
  });
});
