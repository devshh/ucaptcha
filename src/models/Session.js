/**
 * @typedef {Object} Session User session model
 * @property {string} sessionId User session ID
 * @property {string} websiteKey Website ID
 * @property {string} [imageTag] Human readable text image tag
 * @property {string} [token] Token used for verification of session
 */

/**
 * @typedef {Object} DbSession User session model in DB
 * @property {string} sessionId User session ID
 * @property {string} websiteKey Website ID
 * @property {string} image Image URL for challenge
 * @property {number} obj Image object ID
 * @property {number} score How likely is the user to be a human?
 * @property {number} expiry Expiry time of session
 */

// Needed otherwise typedef import doesn't work
export {};
