/**
 * @typedef {Object} DbMat
 * @property {string} file Filename without extension
 * @property {Array<number>} mat Matrix of selected answers
 * @property {number} num Number of users that have seen this image
 * @property {Array<number>} obj The object(s) present in the image
 */

export {};
