/**
 * @typedef {Object} DbToken
 * @property {string} token
 * @property {string} websiteKey
 * @property {number} score
 * @property {number} expiry
 */
export {};
