/**
 * @param {import('express').Response} res
 * @param {JSON} json
 */
export default (res, json) => {
  res.setHeader('Content-Type', 'application/json');
  res.send(`:)${JSON.stringify(json)}`);
};
