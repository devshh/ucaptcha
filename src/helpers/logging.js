const isProd = process.env.NODE_ENV === 'production';

/**
 * @param {*} payload
 */
function info(payload) {
  if (!isProd) {
    console.log('[INFO]:', payload);
  }
}

/**
 * @param {*} payload
 */
function debug(payload) {
  if (!isProd) {
    console.log('[DEBUG]:', payload);
  }
}

export default {
  debug,
  info,
};
