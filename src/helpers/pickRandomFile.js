import fs from 'fs';
import {IMAGES_FOLDER} from '../R.js';
import utils from '../helpers/utils.js';
import logger from './logging.js';

/**
 * @return {Promise<string>} Image file without extension
 */
export default () => {
  return new Promise((resolve, reject)=>{
    fs.readdir(IMAGES_FOLDER, (err, files)=>{
      if (err) return reject(err);

      const filename = utils.randomChoice(files);
      logger.debug({filename});

      return resolve(filename);
    });
  });
};
