import db from '../helpers/db/index.js';

/**
 * serveImage
 * @param {string} sessionId
 * @return {Promise<[string, string]>}
 */
export default async function serveImage(sessionId) {
  /** @type {import('../models/Session.js').DbSession} */
  const session = await db.getSession(sessionId);
  if (!session) {
    // TODO: We need a way to communicate expired session back to client
    return [
      'Session expired',
      null,
    ];
  }

  const {
    image,
  } = session;

  return [
    null,
    image + '.jpg',
  ];
}
