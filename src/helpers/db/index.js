import {
  connect,
  getSession,
  getMat,
  getToken,
  setSession,
  setMat,
  setToken,
  updateSession,
  updateMat,
  deleteSession,
  deleteToken,
} from './sqlite.js'; // Change to `./mongodb.js` to use mongodb.

export default {
  connect,
  getSession,
  getMat,
  getToken,
  setSession,
  setMat,
  setToken,
  updateSession,
  updateMat,
  deleteSession,
  deleteToken,
};
