import _sqlite3 from 'sqlite3';
import _sqlite from 'sqlite';
import cron from 'cron';

const {CronJob} = cron;
const deleteExpiredJob = new CronJob('0 0 2 * * *', () => {
  db.run('DELETE FROM sessions WHERE expiry < ?', [new Date()]);
  db.run('DELETE FROM tokens WHERE expiry < ?', [new Date()]);
}, null, true);


/** @type {import('sqlite').Database | undefined} */
let db;

/** @typedef {import('../../models/Session.js').DbSession} DbSession */
/** @typedef {import('../../models/Token.js').DbToken} DbToken */
/** @typedef {import('../../models/Mat.js').DbMat} DbMat */

/**
 * @typedef {Object} PossibleQueries
 * @property {string} setSession
 * @property {string} updateSession
 * @property {string} getSession
 * @property {string} deleteSession
 *
 * @property {string} setToken
 * @property {string} getToken
 * @property {string} deleteToken
 *
 * @property {string} setMat
 * @property {string} getMat
 * @property {string} updateMat
 */
/** @type {PossibleQueries} */
const query = {};

/**
 * Prepare database and ensure tables are present
 */
function initializeDb() {
  db.configure('busyTimeout', 10000);

  db.run(`
    CREATE TABLE IF NOT EXISTS sessions (
      sessionId BLOB PRIMARY KEY,
      websiteKey BLOB NOT NULL,
      image TEXT NOT NULL,
      obj INT NOT NULL,
      score REAL NOT NULL,
      expiry INTEGER
    );
  `);

  db.run(`
    CREATE TABLE IF NOT EXISTS mats (
      file BLOB PRIMARY KEY,
      mat TEXT NOT NULL,
      num INT NOT NULL,
      obj TEXT NOT NULL
    );
  `);

  // TODO: Also add timestamp
  db.run(`
    CREATE TABLE IF NOT EXISTS tokens (
      token BLOB PRIMARY KEY,
      websiteKey TEXT NOT NULL,
      expiry INTEGER
    );
  `);

  query.setSession = `
    INSERT INTO sessions VALUES (
      $sessionId,
      $websiteKey,
      $image,
      $obj,
      $score,
      $expiry
    );
  `;

  query.updateSession = `
    UPDATE sessions
    SET image = $image,
        obj = $obj,
        score = $score,
        expiry = $expiry
    WHERE sessionId = $sessionId;
  `;

  query.getSession = `
    SELECT * FROM sessions WHERE sessionId = $sessionId LIMIT 1;
  `;

  query.deleteSession = `
    DELETE FROM sessions WHERE sessionId = $sessionId;
  `;

  query.setToken = `
    INSERT INTO tokens VALUES (
      $token,
      $websiteKey,
      $expiry
    );
  `;

  query.getToken = `
    SELECT * FROM tokens WHERE token = $token LIMIT 1;
  `;

  query.deleteToken = `
    DELETE FROM tokens WHERE token = $token;
  `;

  query.getMat = `
    SELECT * FROM mats WHERE file = $file LIMIT 1;
  `;

  query.setMat = `
    INSERT INTO mats VALUES (
      $file,
      $mat,
      $num,
      $obj
    );
  `;

  query.updateMat = `
    UPDATE mats
    SET mat = $mat,
        num = $num,
        obj = $obj
    WHERE file = $file;
  `;
}

/**
 * Connect to the database
 * @param {Function} cb
 */
export async function connect(cb) {
  const _db = await _sqlite.open({
    filename: 'data.sqlite3',
    driver: _sqlite3.Database,
  });
  db = _db;
  // const _db = new sqlite3.Database('data.sqlite3');

  _db.on('open', ()=>{
    db = _db;
    deleteExpiredJob.start();
    initializeDb();
    cb();
  });

  _db.on('error', (err)=>{
    cb(err);
  });
}

/**
 * Set session in DB
 * @param {DbSession} payload
 */
export async function setSession(payload) {
  await db.run(query.setSession, {
    $sessionId: payload.sessionId,
    $websiteKey: payload.websiteKey,
    $image: payload.image,
    $obj: payload.obj,
    $score: payload.score,
    $expiry: payload.expiry,
  });
}

/**
 * Update session in database
 * @param {string} sessionId
 * @param {DbSession} update
 */
export async function updateSession(sessionId, update) {
  await db.run(query.updateSession, {
    $image: update.image,
    $obj: update.obj,
    $score: update.score,
    $expiry: update.expiry,
    $sessionId: sessionId,
  });
}

/**
 * Get session from DB
 * @param {string} sessionId
 * @return {DbSession}
 */
export async function getSession(sessionId) {
  /** @type {DbSession} */
  const row = await db.get(query.getSession, {$sessionId: sessionId});

  if ((row || {}).expiry < Date.now()) {
    await deleteSession(sessionId);
  }
  return row;
}

/**
 * Delete session from DB
 * @param {string} sessionId
 */
export async function deleteSession(sessionId) {
  await db.run(query.deleteSession, {$sessionId: sessionId});
}

/**
 * Set token
 * @param {DbToken} payload
 */
export async function setToken(payload) {
  await db.run(query.setToken, {
    $token: payload.token,
    $websiteKey: payload.websiteKey,
    $expiry: payload.expiry,
  });
}

/**
 * Get token from DB
 * @param {string} token
 * @return {DbToken}
 */
export async function getToken(token) {
  /** @type {DbToken} */
  const row = await db.get(query.getToken, {$token: token});
  if ((row || {}).expiry < Date.now()) {
    await deleteToken(token);
  }
  return row;
}

/**
 * Delete token from DB
 * @param {string} token
 */
export async function deleteToken(token) {
  await db.run(query.deleteToken, {$token: token});
}

/**
 * Set matrix for file in DB
 * @param {DbMat} payload
 */
export async function setMat(payload) {
  await db.run(query.setMat, {
    $file: payload.file,
    $mat: payload.mat.join(','),
    $num: payload.num,
    $obj: payload.obj.join(','),
  });
}

/**
 * Get file matrix
 * @param {string} file Filename
 * @return {DbMat}
 */
export async function getMat(file) {
  /** @type {DbMat} */
  const row = await db.get(query.getMat, {$file: file});
  row.mat = row.mat.split(',').map((m)=>Number(m));
  row.obj = row.obj.split(',').map((o)=>Number(o));

  return row;
}

/**
 * Update file matrix in db
 * @param {string} file
 * @param {DbMat} update
 */
export async function updateMat(file, update) {
  await db.run(query.updateMat, {
    $file: file,
    $mat: update.mat.join(','),
    $num: update.num,
    $obj: update.obj.join(','),
  });
}

export default {
  connect,
  setSession,
  updateSession,
  getSession,
  deleteSession,

  setToken,
  getToken,
  deleteToken,

  setMat,
  updateMat,
  getMat,
};
