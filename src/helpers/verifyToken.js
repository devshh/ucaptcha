import db from '../helpers/db/index.js';
import logger from './logging.js';

/**
 * Verify token from ucaptcha response
 * @param {string} secret
 * @param {string} token
 */
export default async function verifyToken(secret, token) {
  // TODO: We need authentication of the secret key!

  const response = {
    success: false,
    key: null,
    errors: [],
  };

  if (!secret || !token) {
    response.errors.push('INVALID_REQUEST');
    return response;
  }

  const result = await db.getToken(token);
  logger.debug({token: result});
  if (!result) {
    return response;
  }

  await db.deleteToken(token);

  response.success = true;
  response.key = result.websiteKey;
  response.key = result.token;
  return response;
}
