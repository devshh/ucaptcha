import path from 'path';
import {fileURLToPath} from 'url';

export const __dirname = fileURLToPath(import.meta.url);
export const dirname = meta => fileURLToPath(import.meta.url);

export const PROJECT_ROOT = process.cwd();

export const TAGS = [
  null,
  'vehicle',
  'house',
]
/**
 * A path string relative to the project folder.
 * @typedef {String} IMAGES_FOLDER
 */
export const IMAGES_FOLDER = path.join(PROJECT_ROOT, 'public', 'images');

export const MAX_SESSION_TIME = 60 * 30 * 1000;
export const MAX_TOKEN_TIME = 60 * 2 * 1000;


// Anything above this value is considered a true selection within image matrix
export const MAT_TOLERANCE = process.env.NODE_ENV === 'development' ? 999.0 : 0.6;
// A user with a score above this value is considered a human
export const SCORE_TOLERANCE = 0.8;
