import reloadSession from './helpers/reloadSession.js';
import getImage from './helpers/getImage.js';
import verifyImage from './helpers/verifyImage.js';

import {createElement as ce, querySelector as qs} from './util/document.js';

import styles from './res/styles.js';


/**
 * Create a uCaptcha box
 * @param {string} websiteKey Website key
 * @param {Function} [onVerified] Function that will be called on token received
 * @return {HTMLElement} uCaptcha box
 */
function uCaptchaBox(websiteKey, onVerified) {
  /** @type {import('../models/Session.js').Session} */
  let session;

  const styleTag = ce('style');
  styleTag.innerHTML = styles;
  document.head.appendChild(styleTag);

  const captchaBox = ce('div', {id: 'ucaptcha'});

  const checkbox = ce('div', {id: 'uc-checkbox'});
  captchaBox.appendChild(checkbox);

  const captchaContainer = ce('div', {id: 'uc-container', style: 'visibility: hidden'});

  const imageTagTitle = ce('p', {id: 'uc-caption'});
  captchaContainer.appendChild(imageTagTitle);

  const image = ce('img', {id: 'uc-img', style: 'position:absolute;display:block;z-index:-999'});
  captchaContainer.appendChild(image);

  const imageGrid = ce('table', {id: 'uc-grid'});
  for (let i = 0; i < 4; i++) {
    const tr = ce('tr');
    for (let ii = 0; ii < 4; ii++) {
      const td = ce('td',
          {'style': 'cursor:pointer'});
      td.addEventListener('click', (e)=>{
        e.target.classList.toggle('selected');
      });
      tr.appendChild(td);
    }
    imageGrid.appendChild(tr);
  }
  captchaContainer.appendChild(imageGrid);

  const nextBtn = ce('button', {id: 'uc-next'});
  nextBtn.textContent = 'Next';
  nextBtn.addEventListener('click', async (e)=>{
    await verifyImage(session, captchaBox, captchaContainer, onVerified);
  });
  captchaContainer.appendChild(nextBtn);

  const overlay = ce('div', {id: 'uc-overlay', style: 'display: none'});
  document.body.appendChild(overlay);

  document.body.appendChild(captchaContainer);

  captchaBox.addEventListener('click', async ()=>{
    if (!session) {
      session = await reloadSession(websiteKey);
      await getImage(session, captchaContainer);
      qs('#uc-caption').innerHTML = `Select all squares with <b>${session['imageTag']}s</b>`;
    }

    overlay.style.display = 'block';
    captchaContainer.style.visibility = 'visible';
  });

  return captchaBox;
}

/**
 * Instantiate a uCaptcha box
 * @param {string} websiteKey
 * @param {string} selector
 * @param {Function} [onVerified]
 */
export function create(websiteKey, selector, onVerified) {
  // const iframe = ce("iframe");
  // iframe.setAttribute("src", "https://localhost:444/?k="+websiteKey)

  qs(selector).appendChild(uCaptchaBox(websiteKey, onVerified));
}
