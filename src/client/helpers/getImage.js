import request from '../util/request.js';
import {querySelector as qs} from '../util/document.js';

/**
 * @typedef {import('../../models/Session.js').Session} Session
 */

/**
 * Instantiate the uCaptcha loop
 * @param {Session} session
 * @param {HTMLElement} captchaContainer
 */
export default async function getImage(session, captchaContainer) {
  const blob = await request(
      `/api/image?s=${session['sessionId']}`, {_responseType: 'blob'});
  const imageUrl = URL.createObjectURL(blob);

  captchaContainer.querySelector('#uc-img')
      .setAttribute('src', imageUrl);

  return;
}
