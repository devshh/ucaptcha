import request from '../util/request.js';

/**
 * @typedef {import('../../models/Session.js').Session} Session
 */

/**
 * @param {string} websiteKey
 * @return {Promise<Session>}
 */
export default async function reloadSession(websiteKey) {
  /** @type {Session} */
  const session = await request(`/api/reload?k=${websiteKey}`);
  console.info({session});

  return session;
}
