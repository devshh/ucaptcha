import request from '../util/request.js';
import getImage from './getImage.js';
import {querySelector as qs} from '../util/document.js';
import checkmark from '../res/checkmark.js';


/**
 * @typedef {import('../../models/Session.js').Session} Session
 */

/**
 * Verify the image
 * @param {Session} session
 * @param {HTMLElement} captchaBox
 * @param {HTMLElement} captchaContainer
 * @param {Function} [onVerified]
 */
export default async function verifyImage(
    session, captchaBox, captchaContainer, onVerified) {
  const captchaGrid = captchaContainer.querySelector('#uc-grid');

  captchaGrid.classList.add('blurred');
  const tds = captchaGrid.querySelectorAll('td');
  const selectedTds = [];

  const mat = new Array(16);
  for (let i = 0; i < tds.length; i++) {
    if (tds[i].classList.contains('selected')) {
      mat[i] = 1;
      selectedTds.push(tds[i]);
    } else {
      mat[i] = 0;
    }
  }

  const body = {
    's': session['sessionId'],
    'mat': mat,
  };

  /** @type {Session} */
  const resp = await request('/api/verify', {_method: 'POST', _body: body});

  if (resp.token) {
    // FIXME: Doesn't work
    captchaBox.removeEventListener('click', async ()=>{});
    captchaBox.querySelector('#uc-checkbox').innerHTML += checkmark;
    captchaContainer.style.visibility = 'hidden';
    onVerified(resp.token);
    return;
  }

  qs('#uc-caption')
      .innerHTML = `Select all squares with <b>${resp['imageTag']}s</b>`;

  await getImage(session, captchaContainer);

  captchaGrid.classList.remove('blurred');

  selectedTds.map((td)=>{
    td.classList.remove('selected');
  });
}
