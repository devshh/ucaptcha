// For syntax highlighting within VSCode
const styled = {
  div(p) {
    return p;
  },
};

export default styled.div`
  #ucaptcha {
    position: relative;
    display: flex;
    align-items: center;
    padding: 0 24px;
    font-family: Arial;
    border: 1px solid #AAA;
    box-shadow: 0 0 4px 1px rgba(0,0,0,0.08);
    background: #F8F8F8;
    width: 250px;
    height: 70px;
    border-radius: 4px;
  }
  #uc-checkbox {
    width: 25px;
    height: 25px;
    border: 2px solid #BBB;
    background: #FFF; 
    border-radius: 3px;
    cursor: pointer;
  }
  #ucaptcha:active #uc-checkbox {
    background: #EEE;
  }
  #ucaptcha:hover #uc-checkbox {
    background: #FCFCFC;
  }
  #uc-container {
    top: 0;
    position: fixed;
    z-index: 999999;
    background: #FFF;
    padding: 12px;
    transition: 1s ease all;
  }
  #uc-overlay {
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    position: absolute;
    background: rgba(0,0,0,0.4);
    z-index: 99999;
  }
  #uc-next {
    border: none;
    background-color: royalblue;
    color: #FFF;
    padding: 10px 20px;
    text-transform: uppercase;
    border-radius: 3px;
  }
  #uc-grid {
    width: 384px;
    height: 384px;
    transition: .2s ease;
  }
  #uc-grid.blurred {
    background: rgba(255, 255, 255, 0.4);
  }
  #uc-grid td {
    transition: .1s ease;
    cursor: pointer;
    padding: 10px;
  }
  #uc-grid td.selected {
    padding: 0;
    border: 10px solid white;
  }
`;
