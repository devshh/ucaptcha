/**
 * @typedef {Object} RequestOptions
 * @property {Object<string, any>} [_body] Request body
 * @property {string} [_method] Request method
 * @property {XMLHttpRequestResponseType} [_responseType] Response type
 */

/** @type {RequestOptions} */
const defaultOptions = {
  _method: 'GET',
  _responseType: 'text', // Text because we will need to `substr` it.
  _body: undefined,
};

/**
 * Make an HTTP request
 * @param {string} url
 * @param {RequestOptions} [userOptions]
 * @return {Promise} reqestPromise
 */
export async function request(url, userOptions={}) {
  const options = Object.assign({}, defaultOptions, userOptions);
  console.log({options});
  return new Promise((resolve, reject)=>{
    const xhr = new XMLHttpRequest();
    xhr.open(options._method, url);

    xhr.responseType = options._responseType;

    xhr.onload = function() {
      if (this.status < 400) {
        if (options._responseType !== 'text') {
          resolve(this.response);
        } else {
          resolve(JSON.parse(this.responseText.substr(2)));
        }
      } else {
        reject(JSON.parse(this.responseText));
      }
    };

    if (options._method === 'POST') {
      xhr.setRequestHeader('Content-Type', 'application/json');
      xhr.send(JSON.stringify(options._body));
    } else {
      xhr.send();
    }
  });
}
export default request;
