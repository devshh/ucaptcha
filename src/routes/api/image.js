import express from 'express';
import path from 'path';
import {IMAGES_FOLDER} from '../../R.js';
import serveImage from '../../helpers/serveImage.js';

const router = new express.Router();

export default router.get('/image', async (req, res) => {
  const sessionId = req.query.s;
  if (!sessionId) {
    res.end();
    return;
  }

  const [error, filename] = await serveImage(sessionId);

  if (error) {
    res.json({
      error,
    });
    return;
  }

  res.sendFile(path.join(IMAGES_FOLDER, filename));
});
