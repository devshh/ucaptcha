import express from 'express';
import sendJson from '../../helpers/sendJson.js';
import verifyImage from '../../helpers/verifyImage.js';

const router = new express.Router();

/**
 * @param {express.Request} req
 * @param {express.Response} res
 */
const verifyImageHandler = async (req, res)=>{
  const sessionId = req.body.s;
  const mat = req.body.mat;

  let [error, session, actions] = await verifyImage(sessionId, mat);
  actions = actions || {};
  if (error) {
    sendJson(res, {
      error,
    });
    return;
  }

  if (actions.removeCookie) {
    res.clearCookie(actions.removeCookie);
  }

  sendJson(res, session);
};

export default router.post('/verify', verifyImageHandler);
