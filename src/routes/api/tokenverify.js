import express from 'express';
import verifyToken from '../../helpers/verifyToken.js';

const router = new express.Router();

/**
 * @param {express.Request} req
 * @param {express.Response} res
 */
async function tokenVerifyHandler(req, res) {
  const {secret, token} = req.body;

  const result = await verifyToken(secret, token);

  if (result.errors.length) {
    res.status(400).json(result);
    return;
  }

  res.json(result);
}

export default router.post('/tokenverify', tokenVerifyHandler);
