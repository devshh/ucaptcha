import express from 'express';
import sendJson from '../../helpers/sendJson.js';
import {MAX_SESSION_TIME} from '../../R.js';
import reloadSession from '../../helpers/reloadSession.js';

const router = new express.Router();

/**
 * @param {express.Request} req
 * @param {express.Response} res
 */
const reloadSessionHandler = async (req, res) => {
  try {
    const websiteKey = req.query.k;
    if (!websiteKey) {
      res.end();
      return;
    }

    const session = await reloadSession(websiteKey, req.cookies);
    const {sessionId} = session;
    res.cookie(
        websiteKey, sessionId,
        {httpOnly: true, maxAge: MAX_SESSION_TIME * 1000},
    );
    sendJson(res, session);
  } catch (e) {
    console.error(e);
    res.end();
  }
};


export default router.get('/reload', reloadSessionHandler);
