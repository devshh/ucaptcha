import dotenv from 'dotenv';
dotenv.config();

import logger from './helpers/logging.js';

import express from 'express';
const app = express();

app.disable('x-powered-by');

import cookieParser from 'cookie-parser';
app.use(cookieParser(process.env.COOKIE_SECRET));

app.use(express.json({
  limit: '128kb',
}));

if (process.env.NODE_ENV === 'development') {
  app.use((req, _res, next)=>{
    logger.debug(`${req.method} ${req.url}`);
    next();
  });
}

import apiImage from './routes/api/image.js';
import apiVerify from './routes/api/verify.js';
import apiReload from './routes/api/reload.js';
import apiTokenVerify from './routes/api/tokenverify.js';

app.use('/api', [
  apiImage,
  apiVerify,
  apiReload,
  apiTokenVerify,
]);

export {app};
